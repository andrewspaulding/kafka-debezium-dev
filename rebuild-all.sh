#!/bin/bash

cd ~/Documents/gitlab/story/postgres-story-aggregate
sh delete-dev.sh

cd ~/Documents/gitlab/profile/postgres-profile-aggregate
sh delete-dev.sh

cd ~/Documents/gitlab/kafka-debezium-dev
sh delete-dev.sh
sh build-dev.sh

cd ~/Documents/gitlab/profile/postgres-profile-aggregate
sh build-dev.sh

cd ~/Documents/gitlab/story/postgres-story-aggregate
sh build-dev-kafka.sh